const express = require('express')
const router = express.Router()

// Add your routes here - above the module.exports line

// Branching and routing (with a hint of templating)
// /app/views/branching

router.post('/branching/binary-results', function (req, res) {

  // Get the answer from session data
  const likeSmedsAndSmoo = req.session.data['smeds-and-smoos-like']
  // The 'name' in req.session.data['name'] is the same as the 'name' attribute on the HTML input elements
  // Hyphens in JavaScript variables/constants don't work soWeWriteThemLikeThis

  if (likeSmedsAndSmoo === 'no') {
    res.redirect('/branching/bye')
  } else {
    res.redirect('/branching/multiple')
  }
})

router.post('/branching/multiple-results', function (req, res) {

  const favouriteType = req.session.data['favourite-type']

  if (favouriteType === 'smeds') {
    res.redirect('/branching/smeds')
  } else if (favouriteType === 'smoos') {
    res.redirect('/branching/smoos')
  } else {
    res.redirect('/branching/smoo-smed')
  }
})



module.exports = router
